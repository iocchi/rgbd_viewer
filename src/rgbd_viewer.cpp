/*
 * rgbd_viewer.cpp
 *
 * Luca Iocchi 2017
 */

#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>


using namespace std;

void rgbCB(const sensor_msgs::ImageConstPtr& msg){
	cv_bridge::CvImagePtr cv_ptr;

	try{
		cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
	} catch (cv_bridge::Exception& e){
		ROS_ERROR("cv_bridge exception: %s", e.what());
		return;
	}

  cv::imshow("RGB", cv_ptr->image);
  cv::waitKey(10);
}


void depthCB(const sensor_msgs::ImageConstPtr& msg){
	cv_bridge::CvImagePtr cv_ptr;

	try {
		cv_ptr = cv_bridge::toCvCopy(msg, msg->encoding);
	} catch (cv_bridge::Exception& e) {
		ROS_ERROR("cv_bridge exception: %s", e.what());
		return;
	}

  double dmin, dmax;
  cv::minMaxIdx(cv_ptr->image, &dmin, &dmax);
  cv::Mat adjmat;
  cv::convertScaleAbs(cv_ptr->image, adjmat, 255 / dmax);
  cv::imshow("Depth", adjmat);
  cv::waitKey(10);

}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "rgbd_viewer");

	ros::NodeHandle nh, lnh("~");

	string camera_name_rgb, camera_name_depth, topic_rgb, topic_depth;
	lnh.param("camera_name_rgb",camera_name_rgb,string("camera/rgb"));
	lnh.param("camera_name_depth",camera_name_depth,string("camera/depth"));
	topic_rgb = camera_name_rgb+"/image_raw";
	topic_depth = camera_name_depth+"/image_raw";
	
	cout << "Parameters:" << endl;
	cout << "  - camera_name_rgb: " << camera_name_rgb << endl;
	cout << "  - camera_name_depth: " << camera_name_depth << endl;

	cout << "Subscriptions:" << endl;
	cout << "  - RGB topic: " << topic_rgb << endl;
	cout << "  - Depth topic: " << topic_depth << endl;

	ros::Subscriber rgb_sub = nh.subscribe(topic_rgb, 1, &rgbCB);
	ros::Subscriber depth_sub = nh.subscribe(topic_depth, 1, &depthCB);

	ros::spin();

	return 0;
}

