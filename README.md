# README #

Simple example of RGBD image acquisition and visualization.

It uses thin_xtion and thin_kinect ROS nodes for image acquisition

It requires thin_msgs, thix_xtion and thin_kinect from https://bitbucket.org/ggrisetti/thin_drivers

## Use ##

```
$ roslaunch rgbd_viewer rgbd_viewer.launch use_<DEVICE>:=true
```

Available devices: kinect, xtion, pepper

Examples:

```
$ roslaunch rgbd_viewer rgbd_viewer.launch use_xtion:=true
```

```
$ roslaunch rgbd_viewer rgbd_viewer.launch use_pepper:=true camera_name_rgb:=/pepper/camera/front camera_name_depth:=/pepper/camera/depth
```

